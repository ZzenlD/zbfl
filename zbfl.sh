#!/usr/bin/env bash
# shellcheck disable=SC2016,SC2155,SC1090

########################################################## Variablen #########################################################
script_name="$(basename $0)"
script_author="ZzenlD"
script_version="rolling"
script_repository=""
tier_default="0"
tier_count="4"
logfile="${script_name%.*}.log"
config_file="${script_name%.*}.conf"
separator='='
column_width='40'
dependencies_zbfl=("grep" "sed" "awk" "git" "nslookup" "ping" "cut" "tr")
supported_os=("debian" "ubuntu" "archlinux" "centos" "fedora" "alpine")
logo="
${bold} __${reset}
${bold}/   ${blue}${script_name}${reset}
${bold}\__/${reset}\t\t${script_version}
"

# !!! DONT MODIFY VARIABLES !!!
timestamp="$(date "+%Y-%m-%d")"
timestamp_with_time="date \"+%Y-%m-%d %H:%M:%S.%N\""
safeifs="${IFS}"
random_id="$(
  head /dev/urandom | tr -dc A-Za-z0-9 | head -c 16
  echo ''
)"
ug_perms="$(id -un):$(id -gn)"
tier="${tier_default}"
if [[ -f "${config_file}" ]]; then
  source "${config_file}"
fi

### Design ###
export TERM=xterm-256color
bold="$(tput bold)"
reset="$(tput sgr0)"
red="$(tput setaf 1)${bold}"
green="$(tput setaf 2)${bold}"
cyan="$(tput setaf 6)${bold}"
blue="$(tput setaf 4)${bold}"
yellow="$(tput setaf 3)${bold}"

### Variablen einlesen ###
for ((i = 1; i <= $#; i++)); do
  parameter_list+=("$(eval "(echo "\$${i}")")")
done

######################################################### Funktionen #########################################################

# Syntax: write_headline <Tier> <Headline> <Help-Shortcut> <Help-Text>
write_headline() {
  check_failure
  export tier="${1:-${tier}}"
  export tier_master="${tier}"
  local text="$2"
  local shortcut="$3"
  local help_text="$4"
  if [[ -n "${shortcut}" ]] && [[ -n "${help_text}" ]]; then
    write_message "${tier}" "${shortcut}" "${help_text}"
  fi
  echo -e "\\n"
  printf "%*s" "$((tier_count * ((tier_default + tier))))"
  echo -e "${bold}[+] ${blue}${text}${reset}"
  printf "%*s" "$((tier_count * ((tier_default + tier))))"
  #  for ((i = 1; i <= "((${column_width}+5))"; i++)); do
  #    echo -en "${separator}"
  #  done
  for ((i = 1; i <= "${column_width}"; i++)); do
    echo -en "${separator}"
  done
  echo
}

# Syntax: write_topic <Tier> <Topic> <Topic-Modifiziert>
write_topic() {
  export tier="${1:-${tier}}"
  local text="$2"
  local text_mod="${3:-${text}}"
  printf "%*s" "$((tier_count * ((tier_default + tier))))"
  echo -en "${bold}${text_mod}"
  printf "%*s" $((column_width - ${#text} - ((tier_count * ((tier - ${tier_master:-tier})))) - 1))
  echo -en ":${reset} "
}

# Syntax: write_status <Exit-Code> <Meldung> <Hinweis>
write_status() {
  local code="$1"
  case "${code}" in
  0)
    local text="${2:-ok}"
    echo -en "${green}"
    ;;
  1)
    local text="${2:-failed}"
    echo -en "${red}"
    failure=true
    ;;
  *)
    local text="${2:-nothing to do}"
    ;;
  esac
  local hint="$3"
  #  echo -e "${text# }${reset}"
  if [[ -n "${hint}" ]]; then
    echo -en "${text# }${reset}"
    printf "%*s" $((((column_width / 2)) - ${#text}))
    write_message "${tier}" "info" "${hint}"
  else
    echo -e "${text# }${reset}"
  fi
}

# Syntax: write_list <Tier> <Text>
write_list() {
  export tier="${1:-${tier}}"
  local text="$2"
  printf "%*s" "$((tier_count * ((tier_default + tier + 1))))"
  echo -e "- ${text}"
}

# Syntax: write_block <Tier> <Text>
write_block() {
  export tier="${1:-${tier}}"
  local text="$2"
  printf "%*s" "$((tier_count * ((tier_default + tier))))"
  echo -e "${text}"
}

# Syntax: write_ask <Stufe> <Frage> <Varname> <Default-Wert> <Check1,CheckN> <Option1, OptionN>
write_ask() {
  export tier="${1:-${tier}}"
  local text="$2"
  local var_ext="$3"
  local var="$(echo "${var_ext}" | tr -cd '[:alnum:]_')"
  local fr=true
  if ! [[ -n "$(eval "(echo "\$"${var}"")")" ]] || [[ "${var_ext}" =~ ^(\*|-) ]]; then
    if [[ -n "$(eval "(echo "\$"${var}"")")" ]]; then
      local defval="$(eval "(echo "\$"${var}"")")"
    else
      local defval="$4"
    fi
    local checks="$5"
    local options="$6"
    case "${defval}" in
    yes | true)
      defval_mod="${green}${defval}${reset}"
      ;;
    no | false)
      defval_mod="${red}${defval}${reset}"
      ;;
    *)
      defval_mod="${defval}"
      ;;
    esac
    if [[ -n "${defval}" ]]; then
      local topic="${text} [${defval}]"
      local topic_mod="${text} ${reset}[${defval_mod}]"
    else
      local topic="${text}"
      local topic_mod="${topic}"
    fi
    while [[ "${fr}" ]] || [[ -z "$(eval "(echo "\$"${var}"")")" ]]; do
      if [[ "${checks}" == "options" ]] && [[ -n "${options}" ]]; then
        write_message "${tier}" "info" "Options: ${options}"
      fi
      write_topic "${tier}" "${topic}" "${topic_mod}"
      read -r val
      if [[ -n "${val:-${defval}}" ]]; then
        if check_input "${val:-${defval}}" "${checks}"; then
          if [[ "${var_ext}" =~ ^- ]]; then
            export "${var}"="${val:-${defval}}"
          else
            save_value "${var}" "${val:-${defval}}"
          fi
        fi
      fi
      if [[ -z "$(eval "(echo "\$"${var}"")")" ]]; then
        write_message "${tier}" "info" "${text} is required, please enter valid data"
      fi
      unset fr
    done
  else
    save_value "${var}" "$(eval "(echo "\$"${var}"")")"
  fi
}

# Syntax: write_message <Tier> <Type> <Text> <Explicit to stderr>
write_message() {
  export tier="${1:-${tier}}"
  local type="$2"
  local text="$3"
  local explicit_stderr="$4"
  printf "%*s" "$((tier_count * ((tier_default + tier))))"
  case "${type}" in
  info)
    echo -en "${cyan}${bold}[i] "
    ;;
  warning)
    echo -en "${yellow}${bold}[!] "
    ;;
  success)
    echo -en "${green}${bold}[v] "
    ;;
  error)
    if [[ -n "${explicit_stderr}" ]]; then
      echo >&2 "${text}"
    fi
    echo -en "${red}${bold}[x] "
    failure=true
    ;;
  *)
    if [[ -n "${type}" ]]; then
      echo -en "${cyan}[${type}]${reset} "
    fi
    ;;
  esac
  echo -e "${text}${reset}"
}

# Formatiert stderr und schreibt es in log-Datei
write_log() {
  while read line; do
    if [[ -n "${git_branch}" ]] && [[ -n "${git_commit_sha}" ]]; then
      echo "${git_branch}(${git_commit_sha}): $(eval "${timestamp_with_time}") [${random_id}]: [${ug_perms}]: ${red}${line}${reset}" >>"${logfile:-/dev/null}"
    else
      echo "$(eval "${timestamp_with_time}") [${random_id}]: [${ug_perms}]: ${red}${line}${reset}" >>"${logfile:-/dev/null}"
    fi
  done
}

# write_logo <Tier>
write_logo() {
  export tier="${1:-${tier}}"
  clear
  printf %s "$logo" | while read -r line; do
    printf "%*s" "$((tier_count * ((tier_default + tier))))" && echo -e "${line}"
  done
  echo
}

# Zeigt spezifische ID + auftretende Fehler an
write_result() {
  local result_final="$1"
  if [[ -n "${result_final}" ]]; then
    check_failure
  fi
  if [[ "${logfile}" != "" ]]; then
    if [[ "$(grep ${random_id} ${logfile:-/dev/null} | wc -l)" -gt 1 ]]; then
      local error_occured=true
    fi
    if [[ "${headless}" != true ]]; then
      echo -en "\n\nID: ${random_id} - "
      if [[ -z "${error_occured}" ]] && [[ -z "${failure}" ]]; then
        echo "${green}OK${reset}"
      else
        echo "${red}FEHLER${reset}"
        if [ "${error_occured}" ]; then
          for ((i = 1; i <= "${column_width}"; i++)); do
            echo -en "${separator}"
          done
          echo && grep "${random_id}" "${logfile:-/dev/null}"
        fi
      fi
    else
      echo "\"${random_id}\""
    fi
  else
    echo
  fi
  if [[ -f "${config_file}_tmp" ]] && [[ "$(cat ${config_file}_tmp | tail -1 | grep ^PLACEHOLDER)" == "" ]]; then
    mv "${config_file}_tmp" "${config_file}"
  else
    rm "${config_file}_tmp"
  fi
  if [[ -z "${error_occured}" ]] && [[ -z "${failure}" ]]; then
    exit 0
  else
    exit 1
  fi

}

# Tabellen-Kopfzeile
# Syntax: write_table_header <Tier> <Spaltenname1;Spaltenname2;SpaltennameN>
write_table_header() {
  export tier="${1:-${tier}}"
  local input="$2"
  local IFS=";"

  # Spaltenbezeichung in Array einlesen
  for item in ${input}; do
    local array+=("${item}")
  done
  IFS="${safeifs}"

  if [[ "${headless}" != true ]]; then
    echo
    printf "%*s" "$((tier_count * ((tier_default + tier))))"
    for column_name in "${array[@]}"; do
      echo -en "${blue}${bold}${column_name}${reset}" && printf "%*s" $((column_width - ${#column_name} - 2))
      ((column_count++))
    done

    #		IFS="${safeifs}" && echo
    echo

    local amount="$((column_width * column_count))"

    printf "%*s" "$((tier_count * ((tier_default + tier))))"
    for ((i = 1; i <= "${amount}"; i++)); do
      echo -en "${separator}"
    done
    echo
  else
    for ((i = 0; i < "${#array[@]}"; i++)); do
      if [[ "$(($i + 1))" == "${#array[@]}" ]]; then
        echo -en "\"${array[$i]}\""
        break
      else
        echo -en "\"${array[$i]}\","
      fi
    done
    echo
  fi
  unset array
}

# Tabellenzeile
# Syntax: write_table_row <Tier> <Spalte1;Spalte2;SpalteN> <Zeilenfarbe>
write_table_row() {
  export tier="${1:-${tier}}"
  local input="$2"
  local IFS=";"

  # Tabellenfelder in Array einlesen
  for item in ${input}; do
    local array+=("${item}")
  done
  IFS="${safeifs}"

  if [[ "${headless}" != true ]]; then
    local color="$3"
    if [[ -n "${color}" ]]; then
      echo -en $(eval "(echo "\$${color}")")
    fi
    printf "%*s" "$((tier_count * ((tier_default + tier))))"
    for var in "${array[@]}"; do
      echo -en "${var}" && printf "%*s" $((column_width - "${#var}" - 2))
    done

  else
    for ((i = 0; i <= "${#array[@]}"; i++)); do
      if [[ "$(($i + 1))" == "${#array[@]}" ]]; then
        echo -en "\"${array[$i]}\""
        break
      else
        echo -en "\"${array[$i]}\","
      fi
    done
  fi
  unset array && echo "${reset}"
}

# Sytanx: check_failure <Text>
check_failure() {
  local text="$1"
  if [[ -n "${failure}" ]]; then
    write_message "" "error" "${text:-Error occurred. Fix them to continue}"
    echo
    unset failure
    if [[ "${script_repository}" != "" ]]; then
      write_headline "${tier_default}" "Open Issue"
      write_ask "" "Collect Debug-Information" "-COLLECT_DEBUG_INFORMATION" "no" "yesno"
      if [[ "${COLLECT_DEBUG_INFORMATION}" == "yes" ]]; then
        write_message "" "info" "Create an Issue at ${script_repository} with the following content:"
        collect_debug
      fi
    fi
    failure=true
    write_result
  else
    unset failure
  fi
}

# Syntax: check_input <Input> <Check1,CheckN>
check_input() {
  local input="$1"
  local checks="$2"
  local fqdn="$(echo "${input}" | cut -d'/' -f3 | grep -Eoh '[A-Za-z0-9\.\-]+$')"
  for check in ${checks//,/ }; do
    if [[ -z "${failure_check}" ]]; then
      case "${check}" in
      mail)
        if ! echo "${input}" | grep -E '.*@[A-Za-z0-9\.\-]+\.[A-Za-z]+' &>/dev/null; then
          local failure_check=true
        fi
        ;;
      nslookup)
        if ! nslookup "${fqdn}" &>/dev/null; then
          local failure_check=true
        fi
        ;;
      ping)
        if ! ping -c3 "${fqdn}" &>/dev/null; then
          local failure_check=true
        fi
        ;;
      numeric)
        if ! [[ "${input}" =~ ^[0-9]+$ ]]; then
          local failure_check=true
        fi
        ;;
      domain)
        if ! [[ "${input}" =~ ^[A-Za-z0-9\.\-]+[A-Za-z0-9\-]+$ ]]; then
          local failure_check=true
        fi
        ;;
      ip)
        if ! [[ "${input}" =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
          local failure_check=true
        fi
        ;;
      yesno)
        if ! [[ "${input}" =~ ^(yes|no)$ ]]; then
          local failure_check=true
        fi
        ;;
      truefalse)
        if ! [[ "${input}" =~ ^(true|false)$ ]]; then
          local failure_check=true
        fi
        ;;
      confirm)
        if ! [[ "${input}" == "YES" ]]; then
          local failure_check=true
        fi
        ;;
      options)
        if ! [[ "${input}" =~ ^(${options//, /|})$ ]]; then
          local failure_check=true
        fi
        ;;
      port)
        if nc -z "${LISTEN_IP_ADDRESS}" "${input}"; then
          if [[ "$(docker-compose ps 2>/dev/null | grep -Eoh ':[0-9]+' | grep -Eoh "^:${input}$")" == "" ]]; then
            local failure_check=true
          fi
        fi
        ;;
      rsa_key)
        if ! [[ "${input}" =~ ^ssh-rsa[[:space:]][A-Za-z0-9+/]{372,}[[:space:]].*@.*$ ]]; then
          local failure_check=true
        fi
        ;;
      locale)
        if ! [[ "${input}" =~ ^[a-z]{2}_[A-Z]{2}\.UTF-8$ ]]; then
          local failure_check=true
        fi
        ;;
      timezone)
        if ! [[ -f /usr/share/zoneinfo/"${input}" ]]; then
          local failure_check=true
        fi
        ;;
      *) ;;

      esac
    fi
  done
  if [[ -n "${failure_check}" ]]; then
    return 1
  fi
}

# Syntax: check_dependencies <Input>
check_dependencies() {
  for dependency in "${dependencies_zbfl[@]}" "${dependencies[@]}"; do
    if ! command -v "${dependency}" &>/dev/null; then
      failed_dependencies="${failed_dependencies}${dependency}, "
    fi
  done
  if [[ -n "${failed_dependencies}" ]]; then
    write_message "" "error" "The following dependencies must be installed to continue: ${failed_dependencies%%, }" "1"
  fi
}

# Syntax: check_os
check_os() {
  if ! [[ " ${supported_os[@]} " =~ " ${OS} " ]]; then
    write_message "" "error" "OS \"${OS}\" not supported, supported are: $(echo ${supported_os[*]} | sed 's/ /, /g')" "1"
  fi
}

# Syntax: save_value <Name> <Value> <Initial>
save_value() {
  local name="$1"
  local value="$2"
  local initial="$3"
  if [[ "${config_file}" != "" ]]; then
    if [[ "${initial}" == "1" ]]; then
      echo "${name}='${value}'" >"${config_file}_tmp"
    else
      if [[ "$(grep -E "^${name}=" "${config_file}_tmp")" == "" ]]; then
        echo "${name}='${value}'" >>"${config_file}_tmp"
      else
        sed -i "s/^${name}=.*/${name}='${value}'/g" "${config_file}_tmp"
      fi
    fi
  fi
  export "${name}"="${value}"
}

# Syntax: collect_debug
collect_debug() {
  echo "### Debug-Information"
  local commit_hash="$(git show --format="%h" --no-patch)"
  if [[ "$(git status | grep -E "(branch is up|selben Stand)")" ]]; then
    echo "**Commit:** ${commit_hash} \(Up-to-date\)\\\"
  else
    echo "**Commit:** ${commit_hash} \(Update available\)\\\"
  fi
  echo "**Command:** \`./$(basename "$0") ${all_vars}\`"

  # Systeminformation
  echo -e '<details>\n<summary>Systeminformation</summary>\n'
  echo "**Free Space:** $(df -Ph . | awk 'NR==2 {print $4}')\\"
  if selinuxenabled; then
    echo '**SELinux:** enabled\'
  else
    echo '**SELinux:** disabled\'
  fi
  if apparmor_status; then
    echo '**AppArmor:** enabled'
  else
    echo '**AppArmor:** disabled'
  fi
  echo -e '**OS**\n```' && lsb_release -a && echo '```'
  echo -e '**Kernel**\n```' && uname -a && echo '```'
  echo -e '**User**\n```' && id "$(id -u)" && echo '```'
  echo -e '</details>'

  # Packages
  echo -e '<details>\n<summary>Packages</summary>\n'
  echo -e '**bash**\n```' && bash --version | grep -m1 Version && echo '```'
  echo -e '**sed**\n```' && sed --version | head -1 && echo '```'
  echo -e '**awk**\n```' && awk -W version && echo '```'
  case "${DEPLOYMENT}" in
  docker)
    echo -e '**docker**\n```' && docker version && echo '```'
    echo -e '**docker-compose**\n```' && docker-compose version && echo '```'
    ;;
  esac
  echo -e '</details>'

  # Config-File
  echo -e '<details>\n<summary>Configuration</summary>\n\n```bash'
  while read -r line; do
    if [[ $(echo "${line}" | grep -Ei "(email|organization|password|_domain|_key_)") != "" ]]; then
      local var="$(echo "${line}" | grep -Eoh '^\w+=')"
      echo "${line}" | sed "s/${var}.*/${var}'******'/g"
    else
      echo "${line}"
    fi
  done <etc/script/"${script_name}".conf
  echo -e '```\n</details>'
}

# Mögliche Optionen anzeigen
getStarted() {
  write_message "${tier_default}" "Syntax" "${script_name} ${blue}[Argument]${reset}"
  for ((i = 0; i < "${#arguments[@]}"; i = i + 2)); do
    printf "%*s" $((((tier_count * ((tier_default + tier)))) + $(echo "[Syntax]" | wc -m) + ${#script_name} + 4))
    echo -en "${blue}${arguments[i]}${reset}"
    printf "%*s" $(($((column_width / 2)) - ${#arguments[i]}))
    echo -e "${arguments[((i + 1))]}"
  done
  echo
}

# Hilfe einblenden
help() {
  write_headline "${tier_default}" "Befehlszusammenfassung" "Syntax" "${script_name} [Argument] [Option1] [Option2] ... [OptionN]"
  write_table_header "" "Argument;Beschreibung"
  for ((i = 0; i < "${#arguments[@]}"; i++)); do
    write_table_row "" "$(echo ${arguments[i]} | sed 's/\[//g' | sed 's/\]//g');${arguments[((i + 1))]}"
    ((i++))
  done

  write_table_header "" "Option;Beschreibung"
  for ((i = 0; i < "${#options[@]}"; i = i + 3)); do
    write_table_row "" "${options[i]};${options[((i + 2))]}"
  done

  write_message "" "Copyright" "(c) $(date "+%Y") by ${script_author}"
}

# Define some vars before write_log starts
prepare_zbfl() {
  if git rev-parse &>/dev/null; then
    git_branch="$(git rev-parse --abbrev-ref HEAD)"
    git_commit_sha="$(git rev-parse HEAD | cut -c 1-8)"
  fi
  if [[ -e /etc/debian_version ]]; then
    source /etc/os-release
    OS=$ID
  elif [[ -e /etc/fedora-release ]]; then
    source /etc/os-release
    OS=$ID
  elif [[ -e /etc/centos-release ]]; then
    OS=centos
  elif [[ -e /etc/alpine-release ]]; then
    OS=alpine
  elif [[ -e /etc/arch-release ]]; then
    OS=archlinux
  fi
}

init_zbfl() {
  write_logo
  save_value "PLACEHOLDER" "" "1"
  argument="${parameter_list[0]}"
  if [[ -n "${git_branch}" ]] && [[ -n "${git_commit_sha}" ]]; then
    echo -en "${git_branch}(${git_commit_sha}): $(eval ${timestamp_with_time}) [${random_id}]: [${ug_perms}]: cmd='$0 $*' argument='${argument}' " >>"${logfile:-/dev/null}"
  else
    echo -en "$(eval ${timestamp_with_time}) [${random_id}]: [${ug_perms}]: cmd='$0 $*' argument='${argument}' " >>"${logfile:-/dev/null}"
  fi
  check_dependencies
  check_os
  if [[ "${argument}" == "help" ]]; then
    help
    exit 0
  fi
  if [[ "${arguments[*]}" != "" ]] && ! [[ "${argument}" =~ ^⁻.+ ]]; then
    for ((i = 0; i < "${#arguments[@]}"; i = i + 2)); do
      if [[ "${arguments[i]}" == "${argument}" ]]; then
        argument_valid=true
      fi
    done
    if [[ -z "${argument_valid}" ]]; then
      getStarted
      write_result
    fi
  fi
  if [[ "${options[*]}" != "" ]]; then #&& [[ " ${parameter_list[*]} " =~ " ⁻.* " ]]; then
    for ((i = 0; i < ${#options[@]}; i = i + 3)); do
      for ((x = 0; x <= "${#parameter_list[@]}"; x++)); do
        if [[ "${parameter_list[$x]}" == "${options[$i]}" ]]; then
          if [[ "${options[$i]}" =~ ^-[A-Z] ]]; then
            export "${options[$i + 1]}"=true
          else
            export "${options[$i + 1]}"="${parameter_list[$x + 1]}"
          fi
        fi
      done
    done
  fi
  echo >>"${logfile:-/dev/null}"
}

function version_ge() { test "$(echo "$@" | tr " " "\n" | sort -rV | head -n 1)" == "$1"; }
function version_gt() { test "$(printf '%s\n' "$@" | sort -V | head -n 1)" != "$1"; }

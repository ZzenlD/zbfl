# ZzenlD's Bash Functions Library

```bash
#!/usr/bin/env bash

curl -s https://gitlab.com/ZzenlD/zbfl/-/raw/master/zbfl.sh -o /tmp/zbfl.sh
source /tmp/zbfl.sh
#script_name="$(basename $0)"
#script_author="ZzenlD"
#script_version="rolling"
#script_repository=""
#tier_default="0"
#tier_count="4"
#logfile="${script_name%.*}.log"          #logfile="" to not create a log
#config_file="${script_name%.*}.conf"
#separator='='
#column_width='40'
#dependencies=("")
#logo="
#${bold} __${reset}
#${bold}/   ${blue}${script_name}${reset}
#${bold}\__/${reset}\t\t${script_version}
#"
#supported_os=("debian" "ubuntu" "archlinux" "centos" "fedora" "alpine")
#arguments=("create" "sets input from $1 to argument='create'"
#           "update" "sets input from $1 to argument='update'")
#options=("-c" "city" "sets value behind the option to city='\$value', eg. -c Berlin => city='Berlin'"
#         "-a" "animal" "sets value behind the option to animal='\$value', eg. -a dog => animal='dog'"
#         "-H" "headless" "options starting with big letters sets value to true, eg. -H => headless=true")

prepare_zbfl
{
  init_zbfl

  # Insert your code here:
  # ./your_script.sh create -c Berlin
  # if [[ "${argument}" == "create" ]] && [[ "${city}" == "Berlin ]]; then
  # echo "Create city Berlin"
  # fi

  write_result final
} 2> >(write_log)
```

## Functions

### write_message

Syntax: `write_message <Tier> <Message-Shortcut> <Text of the message>`

Message-Shortcut:

- `info` - Cyan text for informations
- `warning` - Yellow text for warnings
- `error` - Red text for errors
- `success` - Green text for successfull messages
- `<Custom text>` - Cyan text for informations but with custom Shortcut-Text

Example 1:

```bash
# write_message "" info "This is an example"
[i] This is an example
||
```

Example 2:

```bash
# write_message 1 Custom "This is an example"
    [Custom] This is an example
||
```

### write_headline

Syntax: `write_headline <Tier> <Text of the headline> [<Message-Shortcut> <Text of the message>]`

Example 1:

```bash
# write_headline "" "This is an example"
[+] This is an example
========================================
||
```

Example 2:

```bash
# write_headline 1 "This is an example with note" info "This is a note"
    [i] This is a note


    [+] This is an example with note
    ========================================
||
```

Example 3:

```bash
# write_headline 2 "This is an example with custom note" Custom "This is a custom note"
        [Custom] This is a custom note


        [+] This is an example with custom note
        ========================================
||
```

### write_topic

Syntax: `write_topic <Tier> <Text of the topic> [<Formatted text of the topic>]`

Example 1:

```bash
# write_topic "" "This is an example topic"
This is an example topic               : ||
```

Example 2:

```bash
# write_topic 1 "This is an example topic with formatted text" "${green}This is an example topic with formatted text${reset}"
    This is an example topic               : ||
```

### write_status

Syntax: `write_status <Exit-Code> [<Text of the status> <Test of the message>]`

Exit-Code:

- `0` - Default text is _ok_ and green, customizable via `<Text of the status>`
- `1` - Default text is _failed_ and red, customizable via `<Text of the status>`
- `<all other>` - Default text is _nothing to do_, customizable via `<Text of the status>`

Example 1:

```bash
# write_topic "" "This is an example topic" && write_status 1
This is an example topic               : failed
||
```

Example 2:

```bash
# write_topic "" "This is an example topic" && write_status 0 successful
This is an example topic               : successful
||
```

Example 3:

```bash
# write_topic "" "This is an example topic" && write_status $? "" "If failed, check ..."
This is an example topic               : ok                  [i] If failed, check ...
||
```

Example 4:

```bash
# write_topic "" "This is an example topic" && write_status "" " "
This is an example topic               :
||
```

### write_list

Syntax: `write_list <Tier> <Text of the list item>`

Example 1:

```bash
# write_list "" example
    - example
||
```

Example 2:

```bash
# write_list 1 example
        - example
||
```

### write_block

Syntax: `write_block <Tier> <Text of the list item>`

Example 1:

```bash
# write_block "" "Very important Textblock"
    Very important Textblock
||
```

Example 2:

```bash
# write_block 1 "Very important Textblock"
        Very important Textblock
||
```

### write_ask

Syntax: `write_ask <Tier> <Text of the question> <Variable-Name> [<Default value> <Checks> <Selection options>]`

Variablen-Name:

- `<Variablen-Name>` - Value is _only queried initially_ and then stored
- `*<Variablen-Name>` - Value is _always queried_ and then stored
- `-<Variablen-Name>` - Value is always queried but _not stored_

Checks:

- `mail` - Checks for valid email address
- `nslookup` - Checks if domain is resolvable
- `ping` - Checks if ip/domain is reachable
- `numeric` - Checks if input is a number
- `domain` - Checks if input is a valid domain
- `ip` - Checks if input is a valid ip
- `yesno` - Checks if input is _yes_ or _no_
- `truefalse` - Checks if input is _true_ or _false_
- `confirm` - Checks if input is _YES_
- `options` - Checks if input corresponds to one of the values specified at `<Selection options>`
- `port` - Checks if input is a valid port
- `rsa_key` - Checks if input is a valid rsa_key
- `locale` - Checks if input is a valid locale, matching _UTF-8_
- `timezone` - Checks if input is a valid timezone

Example 1:

```bash
# write_ask "" "This is a question" VARIABLE_NAME
This is a question                     : ||
```

Example 2:

```bash
# write_ask 1 "This is a question" *VARIABLE_NAME default.value domain,ping
    This is a question [default.value]           : ||
```

Example 3:

```bash
# write_ask 2 "This is a question" -VARIABLE_NAME "" options "select1, select2"
        [i] Options: select1, select2
        This is a question                     : ||
```

### write_table_header

Syntax: `write_table_header <Tier> <Text of the table columns>`

Example 1:

```bash
# write_table_header "" "Column1;Column2;Column3"
Column1                               Column2                               Column3
========================================================================================================================
||
```

Example 2:

```bash
# write_table_header 1 "Column1;Column2;Column3"
    Column1                               Column2                               Column3
    ========================================================================================================================
||
```

### write_table_row

Syntax: `write_table_row <Tier> <Text of the table rows> [<Color of the row>]`

Example 1:

```bash
# write_table_row "" "Text1;Text2;Text3"
Text1                                 Text2                                 Text3
||
```

Example 2:

```bash
# write_table_row 1 "Text1;Text2;Text3" green
    Text1                                 Text2                                 Text3
||
```
